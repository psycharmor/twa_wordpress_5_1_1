<?php
/*
    Name: register_body.php
    Description:
        Create the base class of the registration page's body element
*/

namespace triwest_portal;

class Register_Body {

    private function create_select_tag_from_items( $items ) {
        /*
            Create multiple select tags from a list of $items
            @params:
                $items          -> array( array() )
            @return:
                string
        */

        $content = "";
        foreach( $items as $item )
        {
            $option_label = $item[0];
            $value = $item[1];
            $content .= "<option value='$value'>$option_label</option>";
        }

        return $content;
    }

    protected function create_title_and_subtitle_element( $title, $subtitle ) {
        /*
            Create the elements that hold the title and subtitles of each window
            @params:
                $title          -> string
                $subtitle       -> string
            @return:
                string
        */

        $content = "";

        $content .= "<div class='pai-title-container'>";
            $content .= "<h2 class='pai-window-title'>$title</h2>";
            $content .= "<h3 class='pai-window-subtitle'>$subtitle</h3>";
        $content .= "</div>"; // pai-title-container;

        return $content;
    }

    protected function create_input_element( $type, $id, $label, $placeholder, $msg, $disabled ) {
        /*
            Create a basic input element with input verification
            @params:
                $type           -> string
                $id             -> string
                $label          -> string
                $placeholder    -> string
                $msg            -> string
                $disabled       -> string
            @return:
                string
        */

        $max_length = "255";
        $content = "";

        $content .= "<div class='form-group pai-form-group'>";
            $content .= "<div class='pai-label-container'>";
                $content .= "<label class='pai-form-label' for='$id'>$label</label>";
            $content .= "</div>"; // pai-label-container
            $content .= "<input class='form-control pai-form-control' type='$type' id='$id' name='$id' placeholder='$placeholder' maxlength='$max_length' $disabled>";
            $content .= "<div class='invalid-feedback' id='$id'>";
                $content .= $msg;
            $content .= "</div>"; // invalid-feedback
        $content .= "</div>"; // form-group pai-form-group

        return $content;
    }

    protected function create_select_element( $items, $id, $label, $placeholder, $msg ) {
        /*
            Create a basic select element with select verification
            @params:
                $items          -> array( array() )
                $id             -> string
                $label          -> string
                $placeholder    -> string
                $msg            -> string
            @return:
                string
        */

        $content = "";
        $content .= "<div class='form-group pai-form-group'>";
            $content .= "<div class='pai-label-container'>";
                $content .= "<label class='pai-form-label' for='$id'>$label</label>";
            $content .= "</div>"; // pai-label-container
            $content .= "<select class='form-control pai-form-control' id='$id' name='$id'>";
                $content .= "<option value='0'>$placeholder</option>";
                $content .= $this->create_select_tag_from_items( $items );
            $content .= "</select>"; // form-control
            $content .= "<div class='invalid-feedback' id='$id'>";
                $content .= $msg;
            $content .= "</div>"; // invalid-feedback
        $content .= "</div>"; // form-group

        return $content;
    }

    protected function create_multi_col_fields( $elements, $row_id ) {
        /*
            Based on the amount of elements in $elements, create one row
            of equal columns
            @params:
                $elements           -> array( string )
                $row_class          -> string
            @return:
                string
        */

        $size = 12 / count( $elements ); // assumes even division of 12

        $content = "";

        $content .= "<div class='row' $row_id>";
            foreach( $elements as $element ) {
                $content .= "<div class='col-12 col-sm-$size pai-col'>";
                    $content .= $element;
                $content .= "</div>"; // col-12 col-sm-xx
            }
        $content .= "</div>"; // row

        return $content;
    }

    protected function create_checkbox_element( $id, $checkbox_content ) {
        /*
            Create the checkbox element using the content given
            @params:
                $id             -> string
                $checkbox_content        -> string
            @return:
                string
        */

        $content = "";

        $content .= "<label class='pai-checkbox-container'>";
            $content .= $checkbox_content;
            $content .= "<input id='$id' name='$id' type='checkbox'>";
            $content .= "<span class='pai-checkmark'></span>";
        $content .= "</label>"; // pai-checkbox-container

        return $content;
    }

    public function create_alert_message() {
        /*
            Create the elements that make up the alert message that shows
            when the button click fails
            @params:
                None
            @return:
                string
        */

        $content = "";

        $content .= "<div class='pai-alert-container'>";
            $content .= "<div class='alert alert-danger' role='alert'>";
                $content .= "<button type='button' class='close'>";
                    $content .= "<span>&times;</span>";
                $content .= "</button>";
                $content .= "<p></p>"; // this will hold the msg. Left empty on start
            $content .= "</div>"; // alert alert-danger
        $content .= "</div>"; // pai-alert-container

        return $content;
    }

    public function create_btn( $label ) {
        /*
            Create the button that handles sending the user to the next window
            @params:
                $label          -> string
            @return:
                string
        */

        $content = "";
        $content .= "<button type='submit' class='btn pai-btn'>$label</button>";

        return $content;
    }

    public function create_indicators( $amount ) {
        /*
            Create the indicators that show the timeline of the windows
            @params:
                $amount         -> int
            @return:
                string
        */

        $content = "";

        $content .= "<div class='pai-indicators'>";
            $content .= "<div class='pai-step active'></div>";
            for( $i = 1; $i < $amount; ++$i ) {
                $content .= "<hr class='pai-step-sep'>";
                $content .= "<div class='pai-step'></div>";
            }
        $content .= "</div>"; // pai-indicators

        return $content;
    }
}
