<?php
/*
    Name: opt_out_window.php
    Description:
        Create the class that creates the opt out window section of the
        registration page
*/

namespace triwest_portal;

include_once ( get_stylesheet_directory() . "/modules/register/src/php/register_body.php" );

class Opt_Out_Window extends Register_Body {

    private function create_opioid_checkbox() {
        /*
            Create a checkbox element dedicated to the opioid content
            @params:
                None
            @return:
                string
        */

        $header = "Opt-Out: Opioid Safety and Prescribing Best Practices";

        $content = "";

        $content .= "<p class='pai-checkbox-header'>";
            $content .= $header;
        $content .= "</p>"; // pai-checkbox-header
        $content .= "<ol class='pai-checkbox-content bold'>";
            $content .= "<li><span class='pai-checkbox-content small-margin'>I possess a current, active and unrestricted license in a state that requires three (3) or more hours of training every two (2) years on opioid prescribing or pain management;</span></li>";
            $content .= "<span class='pai-in-between-list'>OR</span>";
            $content .= "<li><span class='pai-checkbox-content small-margin'>I do not have a DEA number or prescribe medication as part of my scope of practice.</span></li>";
        $content .= "</ol>"; // pai-checkbox-content bold

        return $this->create_checkbox_element( "opt-out-opioid", $content );
    }

    private function create_gcs_checkbox() {
        /*
            Create a checkbox element dedicated to the
            general competency standards content
            @params:
                None
            @return:
                string
        */

        $header = "Opt-Out: Military and Veteran Culture";

        $content = "";

        $content .= "<p class='pai-checkbox-header'>";
            $content .= $header;
        $content .= "</p>"; // pai-checkbox-header
        $content .= "<ol class='pai-checkbox-content bold'>";
            $content .= "<li><span class='pai-checkbox-content small-margin'>I have completed 500 hours of VA training via an internship, post-doctorate, residency, or similar program;</span></li>";
            $content .= "<span class='pai-in-between-list'>OR</span>";
            $content .= "<li><span class='pai-checkbox-content small-margin'>I have been employed by either VA or the Department of Defense (DoD) with a total of VA/DoD experience of at least one (1) year;</span></li>";
            $content .= "<span class='pai-in-between-list'>OR</span>";
            $content .= "<li><span class='pai-checkbox-content'>I practice within my scope as part of an Academic Affiliate or Center of Excellence.</span></li>";
        $content .= "</ol>"; // pai-checkbox-content bold

        return $this->create_checkbox_element( "opt-out-gcs", $content );
    }

    private function create_no_opt_out_checkbox() {
        /*
            Create a checkbox element dedicated to the
            no opt out checkbox
            @params:
                None
            @return:
                string
        */

        $header = "Continue to VA-Required Training";

        $content = "";

        $content .= "<p class='pai-checkbox-header'>";
            $content .= $header;
        $content .= "</p>"; // pai-checkbox-header
        $content .= "<ol class='pai-checkbox-content bold'>";
            $content .= "<li><span class='pai-checkbox-content small-margin'>I do not self-attest to either of the opt-out criteria above.</span></li>";
        $content .= "</ol>"; // pai-checkbox-content bold

        return $this->create_checkbox_element( "opt-out-none", $content );
    }

    public function create_window() {
        /*
            Create the window that will ask the user to check mark any option
            or none depending on their situation
            @params:
                None
            @return:
                string
        */

        $title = "Continue to Training OR Opt-Out Through Attestation";
        $subtitle = "Under VA guidelines, you are eligible to “opt-out” of the military and
            Veteran culture and/or the opioid safety training if you meet certain
            criteria. Please review the self-attest criteria below and select the
            applicable box(es) to continue.";
        $initial_checkbox_header = "I do not meet the criteria for Opt Out for either training course modules and wish to start the required training now — Select Enroll.";

        $content = "";

        $content .= "<div class='form-group window'>";
            $content .= "<div class='pai-blue-border'>";
            $content .= "</div>"; // pai-blue-border
            $content .= "<div class='pai-fields'>";
                $content .= $this->create_title_and_subtitle_element( $title, $subtitle );
                $content .= $this->create_opioid_checkbox();
                $content .= $this->create_gcs_checkbox();
                $content .= $this->create_no_opt_out_checkbox();
            $content .= "</div>"; //pai-fields
        $content .= "</div>"; // form-group window

        return $content;
    }
}
