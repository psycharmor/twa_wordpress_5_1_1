<?php
/*
    Name: validation_window.php
    Description:
        Create the class that creates the validation window section of the
        registration page
*/

namespace triwest_portal;

include_once ( get_stylesheet_directory() . "/modules/register/src/php/register_body.php" );
include_once ( get_stylesheet_directory() . "/modules/src/php/page_template.php" );

class Validation_Window extends Register_Body {

    private $page_template;

    public function __construct() {
        /*
            Setup all the member variables of the class
            @params:
                None
            @return:
                None
        */

        $this->page_template = new Page_Template();
    }

    public function create_window() {
        /*
            Create the window that asks for the following fields:
                NPI-1 Number
                First Name
                Last Name
            @params:
                None
            @return:
                string
        */

        $title = "VA Training Provider Registration";
        $subtitle = "Welcome to the registration page for the Department of Veterans
            Affairs (VA) required training on military and Veteran culture and
            opioid safety.<br><br>
            To begin, complete the form below and then select “Verify”.";

        $content = "";

        $content .= "<div class='form-group window active'>";
            $content .= $this->page_template->create_header_img();
            $content .= "<div class='pai-blue-border'>";
            $content .= "</div>"; // pai-blue-border
            $content .= "<div class='pai-fields'>";
                $content .= $this->create_title_and_subtitle_element( $title, $subtitle );
                $content .= $this->create_input_element( "number", "npi-number", "NPI-1 Number", "0123456789", "Please enter your NPI-1 number", "" );
                $content .= $this->create_input_element( "text", "first-name", "First Name", "John", "Please enter your first name", "" );
                $content .= $this->create_input_element( "text", "last-name", "Last Name", "Doe", "Please enter your last name", "" );
            $content .= "</div>"; //pai-fields
        $content .= "</div>"; // form-group window active

        return $content;
    }
}
