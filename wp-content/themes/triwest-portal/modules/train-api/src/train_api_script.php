<?php
/*
    Name: train_api_script.php
    Description:
        The script that will run the train api handler. To be used for a cron job
*/

include_once ( get_stylesheet_directory() . "/modules/train-api/src/php/train_api.php" );

function run_train_api() {
    /*
        Get the current time and check for all train activity after
        1 hour before the current time
        @params:
            none
        @return:
            none
    */

    $current_time = time();

    try {
        $file_path = get_stylesheet_directory() . "/modules/train-api/lib/";

        $updated_after = 0;
        if( file_exists( $file_path . "last_run_time.txt" ) ) {
            $fp = fopen( $file_path . "last_run_time.txt", "r" );

            // make updated_after the max between the (last_run_time - 60) and 0
            $updated_after = max( 0, intval( fgets( $fp ) ) - 60 );
            fclose( $fp );

        }

        $updated_after = date( "Y-m-d\TH:i:s", $updated_after );
    }

    catch( Exception $e ) {
        $log_file_path = get_stylesheet_directory() . "/logs/train-api/train_api_log.txt";
        pai_log_errors( $log_file_path, dirname( __FILE__ ), "run_train_api()", $e->getLine(), $e->getMessage() );
    }

    // run user matching
    $train_api = new \triwest_portal\Train_Api_Manager( $updated_after );
    try {
        $train_api->handle_train_to_pai_user_matching( $file_path . "train_users.csv" );
        gc_collect_cycles();
    }

    catch( Exception $e ) {
        // logged within train_api
        return;
    }

    // run activity insertions
    try {
        if( file_exists( $file_path . "train_users.csv" ) ) {
            $train_api->handle_train_activity_recording( $file_path );
        }
    }

    catch( Exception $e ) {
        // logged within train_api
        return;
    }

    $run_time = fopen( $file_path . "last_run_time.txt", "w" );
    fwrite( $run_time, $current_time . "\n" );
    fclose( $run_time );
}
add_action( "triwest_portal_to_train_api", "run_train_api" );
