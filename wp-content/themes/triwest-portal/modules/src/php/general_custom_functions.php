<?php
/*
    Name: general_custom_functions.php
    Description:
        All functions that are used throughout the entire application
*/

function setup_log_error( $file_path, $dir_name, $func_name, $lines, $msg ) {
    /*
        set up the logging procedure.
        1. Update the given file_path to the log to have the current date
        2. create the message that will be written to the log
        @params:
            $file_path          -> string
            $dir_name           -> string
            $func_name          -> string
            $lines              -> string
            $msg                -> string
        @return:
            array
    */
    $pst = new \DateTimeZone( "America/Los_Angeles" );
    $current_datetime = new \DateTime( "now", $pst );

    $timestamp = $current_datetime->format( "Y-m-d H:i:s" );
    $file_path = substr( $file_path, 0, -4 ) . "_{$current_datetime->format( "Y-m-d" )}.txt";

    $msg = "$timestamp; $dir_name; $func_name; line(s) $lines: $msg\n";

    return array( $file_path, $msg );
}

function pai_log_errors( $file_path, $dir_name, $func_name, $lines, $msg ) {
    /*
        Setup the error logging then log the error msg line to the specified
        file_path. $msg should always be formatted in the following fashion
        but can be tailored to any format:
            "datetime; path-to-file; function; line(s): description"
        $file_path should always add to the log file of the current date.
        e.g. ".../train_api_log_2019-0-16.txt"
        @params:
            $file_path          -> string
            $dir_name           -> string
            $func_name          -> string
            $lines              -> string
            $msg                -> string
        @return:
            array
    */

    list( $file_path, $msg ) = setup_log_error( $file_path, $dir_name, $func_name, $lines, $msg );

    $f_open = fopen( $file_path, "a" );
    fwrite( $f_open, $msg );
    fclose( $f_open );
}
