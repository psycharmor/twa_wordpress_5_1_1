<?php
/*
    Name: hooks_and_filters.php
    Description:
        All functions that add new actions or filters related to general
        workpress actions/hooks.
*/


/* redirect login to user's dashboard */
function pai_login_redirect( $redirect_to, $request, $user ) {
    /*
        @params:
            $redirect_to        -> string
            $request            -> string
            $user               -> WP_User, WP_Error
        @return:
            string
    */

    return home_url( "/my-dashboard/" );
}
add_filter( "login_redirect", "pai_login_redirect", 10, 3 );

/* redirect logout to home page */
function pai_logout_redirect( $redirect_to, $request, $user ) {
    /*
        @params:
            $redirect_to        -> string
            $request            -> string
            $user               -> WP_User, WP_Error
        @return:
            string
    */

    return home_url();
}
add_filter( "logout_redirect", "pai_logout_redirect", 10, 3 );

/* hide the admin bar for all except for admins */
function hide_admin_bar() {
    return current_user_can('manage_options');
}
add_filter('show_admin_bar', 'hide_admin_bar');
