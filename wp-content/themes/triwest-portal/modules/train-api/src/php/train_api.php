<?php
/*
    Name: train_api.php
    Description:
        Hold the class that creates the handler for the Train -> PsychArmor API
        This class will read from the Train API and obtain the user course
        activity
*/

namespace triwest_portal;

class Train_Api_Manager {

    private $updated_after;
    private $train_token;

    public function __construct( $updated_after ) {
        /*
            Setup all the member variables of the class
            @params:
                None
            @return:
                None
        */

        $this->updated_after = $updated_after;
        $this->train_token = "3A7D1589-52C7-440E-AB4D-E46CF8D0D402";
    }

    private function get_train_api_result( $url ) {
        /*
            Continously try to make a GET request to Train API and get the results
            @params:
                $url            -> string
            @return:
                array
        */

        $timeout_limit = 30;

        $headers = array(
            "Accept"            => "application/json",
            "Authorization"     => "Token $this->train_token"
        );
        $args = array(
            "headers"       => $headers,
            "timeout"       => $timeout_limit
        );

        // keep trying get request until it's successful
        $attempts = 0;
        $attempt_limit = 2;
        do {
            ++$attempts;
            $results = null;
            $results = wp_remote_get( $url, $args );
            // taking too long
            if( $attempts % $attempt_limit == 0 ) {
                $log_file_path = get_stylesheet_directory() . "/logs/train-api/train_api_log.txt";
                pai_log_errors( $log_file_path, dirname( __FILE__ ), "Train_Api_Manager->get_train_api_result()", "53-70", "Requesting $url is taking too long. Logged at attempt: $attempts" );
            }
        }
        while( is_wp_error( $results ) || $results["response"]["code"] != 200 );

        return json_decode( $results["body"], true );
    }

    private function match_train_users_to_pai_users( &$users, $users_file_path ) {
        /*
            Match train users by email to the psycharmor users and store the users
            into a csv file
            @params:
                $users              -> array
                $users_file_path    -> string
            @return:
                array
        */

        try {
            $matched_users = array();
            if( file_exists( $users_file_path ) ) {
                $matched_users = array_column( array_map('str_getcsv', file( $users_file_path ) ), 1, 0 );
            }

            $fp = fopen( $users_file_path, "a" );
            foreach( $users as $train_user ) {
                $pai_user = get_user_by( "email", $train_user["email"] );
                if( $pai_user !== false ) {
                    if( !array_key_exists( $pai_user->ID, $matched_users ) ) {
                        $pai_id = $pai_user->ID;
                        $train_id = $train_user["id"];
                        fwrite( $fp, "$pai_id,$train_id\n" );
                    }
                }
                $pai_user = null;
            }

            fclose( $fp );
        }

        catch( Exception $e ) {
            $log_file_path = get_stylesheet_directory() . "/logs/train-api/train_api_log.txt";
            pai_log_errors( $log_file_path, dirname( __FILE__ ), "Train_Api_Manager->match_train_users_to_pai_users()", $e->getLine(), $e->getMessage() );
            throw $e;
        }
    }

    private function get_post_by_meta( $key, $value ) {
        /*
            Find all posts given meta key and value
            @params:
                $key            -> string
                $value          -> string
            @return:
                array
        */

        $args = array(
            "post_type"         => 'sfwd-courses',
            "meta_key"          => $key,
            "meta_value"        => $value
        );

        try {
            return get_posts( $args );
        }

        catch( Exception $e ) {
            $file_path = get_stylesheet_directory() . "/logs/train-api/train_api_log.txt";
            pai_log_errors( $file_path, dirname( __FILE__ ), "Train_Api_Manager->get_post_by_meta()", $e->getLine(), $e->getMessage() );
            throw $e;
        }
    }

    private function add_to_csv( $fp_course_access, $fp_course_completions, $fp_user_meta, $course_id, $user, $started_date, $completed_date ) {
        /*
            add to csv if the user has not completed the course according to
            our database
            @params:
                $fp_course_access           -> file pointer
                $fp_course_completions      -> file pointer
                $fp_user_meta               -> file pointer
                $course_id                  -> int
                $user                       -> WP_User
                $started_date               -> int
                $completed_date             -> int
            @return:
                None
        */

        $user_id = $user->ID;

        if( empty( get_user_meta( $user_id, "course_completed_$course_id", true ) ) ) {
            // return;
        }

        // if the user is not already enrolled, enroll them
        if( !empty( get_user_meta( $user_id, "course_" . $course_id . "_access_from", true ) ) ) {
            // access
            $row = array(
                $user_id,
                $course_id,
                0,
                "access",
                $started_date,
                $started_date,
            );
            fputcsv( $fp_course_access, $row );

            // course access meta
            $row = array(
                $user_id,
                "course_" . $course_id . "_access_from",
                $started_date
            );
            fputcsv( $fp_user_meta, $row );
        }

        // add the course completion to the csv
        $row = array(
            $user_id,
            $course_id,
            $course_id,
            "course",
            true,
            $started_date,
            $completed_date,
            $completed_date,
        );
        fputcsv( $fp_course_completions, $row );

        // course completed
        $row = array(
            $user_id,
            "course_completed_$course_id",
            $completed_date
        );
        fputcsv( $fp_user_meta, $row );

        // email
        $course = get_the_title( $course_id );
    	$link = pai_get_course_certificate_link( $course_id, $user_id );

    	if ( !empty( $link ) ) {
            $survey = "https://www.surveymonkey.com/r/JF7P3LM";
            $header = home_url( "/wp-content/uploads/2019/08/Email-Header-Final.png" );
            $footer = home_url( "/wp-content/uploads/2019/08/Email-Footer-Final.png" );

            $headers = array( "Content-Type: text/html; charset=UTF-8" );
            $msg = "<img style='display: block; width: 100%;' src='$header' alt='TriWest header'><br><br>";
    		$msg .= "Congratulations! You earned a certificate for completing the VA-required training for providers. You can download your certificate with the link below:<br><br>";
    		$msg .= "<a href='$link' target='_blank'>$link</a><br><br>";
            $msg .= "While you’re here, we’d like your feedback on how you heard about the VA training. Would you take less than a minute to answer ONE question by clicking below?<br><br>";
    		$msg .= "<a href='$survey' target='_blank'>$survey</a><br><br>";
            $msg .= "Veterans sacrificed and trained to serve us. Thank you for returning the favor by training to serve them.<br><br>";
    		$msg .= "SIGNATURE<br><br>";
            $msg .= "<img style='display: block; width: 100%;' src='$footer' alt='TriWest footer'>";
    		wp_mail($user->user_email, "Download Your Certificate for the VA Training!", $msg, $headers);
    	}
    }

    private function check_course_completion_and_add_to_csv( &$activity, $pai_id, $fp_course_access, $fp_course_completions, $fp_user_meta ) {
        /*
            Check if the course activity is completed and if so, add to necessary
            data to the csvs
            @params:
                $activity                   -> array
                $pai_id                     -> int
                $fp_course_access           -> file pointer
                $fp_course_completions      -> file pointer
                $fp_user_meta               -> file pointer
            @return:
                None
        */

        try {
            if( $activity["statusName"] === "Completed" ) {
                $course = $this->get_post_by_meta( "train_course_id", $activity["courseId"] );
                if( !empty( $course ) ) {
                    $course_id = $course[0]->ID;
                    $user = get_user_by( "id", $pai_id );
                    $started_date = strtotime( $activity["createdDate"] );
                    $completed_date = strtotime( $activity["completionDate"] );

                    $this->add_to_csv( $fp_course_access, $fp_course_completions, $fp_user_meta, $course_id, $user, $started_date, $completed_date );
                }
            }
        }

        catch( Exception $e ) {
            $log_file_path = get_stylesheet_directory() . "/logs/train-api/train_api_log.txt";
            pai_log_errors( $log_file_path, dirname( __FILE__ ), "Train_Api_Manager->check_course_completion_and_add_to_csv()", $e->getLine(), $e->getMessage() );
            throw $e;
        }
    }

    private function load_to_sql( $file_path, $table_name, $fields ) {
        /*
            Use a file to load rows into a table
            @params:
                $file_path          -> string
                $table_name         -> string
                $fields             -> string
            @return:
                None
        */

        if( file_exists( $file_path ) ) {
            global $wpdb;

            $table_name = $wpdb->prefix . $table_name;
            $sql = "
                LOAD DATA LOCAL INFILE '$file_path'
                INTO TABLE $table_name
                FIELDS TERMINATED BY ','
                LINES TERMINATED BY '\n'
                $fields
            ";

            $wpdb->query( $sql );
        }
    }

    private function update_database( $file_path ) {
        /*
            Get all the files and load them onto the database
            @params:
                $file_path          -> string
            @return:
                None
        */

        // course access
        $fields = "(user_id,post_id,course_id,activity_type,activity_started,activity_updated)";
        $this->load_to_sql( $file_path  . "course_access.csv", "learndash_user_activity", $fields );
        unlink( $file_path  . "course_access.csv" );

        // course completions
        $fields = "(user_id,post_id,course_id,activity_type,activity_status,activity_started,activity_updated,activity_completed)";
        $this->load_to_sql( $file_path  . "course_completions.csv", "learndash_user_activity", $fields );
        unlink( $file_path  . "course_completions.csv" );

        // user meta
        $fields = "(user_id,meta_key,meta_value)";
        $this->load_to_sql( $file_path  . "user_meta.csv", "usermeta", $fields );
        unlink( $file_path  . "user_meta.csv" );


    }

    private function parse_registrations_and_update_database( &$activities, $file_path ) {
        /*
            go through each registrations and add them to the database if
            it is completed and it is a course on our system
            @params:
                $activity           -> array
                $file_path          -> string
            @return:
                None
        */

        $matched_users = array_column( array_map('str_getcsv', file( $file_path . "train_users.csv" ) ), 0, 1 );

        $fp_course_access = fopen( $file_path  . "course_access.csv", "w" );
        $fp_course_completions = fopen( $file_path  . "course_completions.csv", "w" );
        $fp_user_meta = fopen( $file_path  . "user_meta.csv", "w" );

        foreach( $activities as $activity ) {
            if( array_key_exists( $activity["userId"], $matched_users ) ) {
                $pai_id = intval( $matched_users[ $activity["userId"] ] );
                $this->check_course_completion_and_add_to_csv( $activity, $pai_id, $fp_course_access, $fp_course_completions, $fp_user_meta );
            }
        }

        fclose( $fp_course_access );
        fclose( $fp_course_completions );
        fclose( $fp_user_meta );

        $this->update_database( $file_path );
    }

    public function handle_train_to_pai_user_matching( $users_file_path ) {
        /*
            Run functions that handle getting train users and matching them
            to PAI users
            @params:
                $users_file_path        -> string
            @return:
                None
        */

        $url = "https://vhama-gov.testing.kmionline.com/api/users/?updatedAfter=$this->updated_after";
        $users = $this->get_train_api_result( $url );
        $this->match_train_users_to_pai_users( $users, $users_file_path );

        $users = null;
        gc_collect_cycles();
    }

    public function handle_train_activity_recording( $file_path ) {
        /*
            Run the functions that handle getting train course activity and
            recording course completions into PAI database. Return the count
            of activity that was given by API
            @params:
                $file_path          -> string
            @return:
                None
        */

        $url = "https://vhama-gov.testing.kmionline.com/api/registrations/?updatedAfter=$this->updated_after";
        $activity = $this->get_train_api_result( $url );

        try {
            $this->parse_registrations_and_update_database( $activity, $file_path );
        }

        catch( Exception $e ) {
            $log_file_path = get_stylesheet_directory() . "/logs/train-api/train_api_log.txt";
            pai_log_errors( $log_file_path, dirname( __FILE__ ), "Train_Api_Manager->handle_train_activity_recording()", $e->getLine(), $e->getMessage() );
            throw $e;
        }
    }
}
