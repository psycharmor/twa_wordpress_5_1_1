"use strict";
/*
    Handle the verify window input verification
    Verification ruleset:
        - only check if the input box contains user input
            - only let the user know if a healthcare provider cannot be found
            - purposely ambiguous for security reasons
*/

$j( document ).ready(function() {
    /*
        Trigger events related to user input
    */

    $j( "input#npi-number" ).blur( function() {
        input_field_filled( $j( this ) );
    });

    $j( "input#first-name" ).blur( function() {
        input_field_filled( $j( this ) );
    });

    $j( "input#last-name" ).blur( function() {
        input_field_filled( $j( this ) );
    });
});

function handle_verify_window() {
    /*
        Handles the 2 part check of the verify window:
            1. Check if the fields are valid and show invalidation messages for
               any invalid input
            2. Use the fields to check against NPPES if the healthcare provider
               exists.
    */

    if( verify_window_fields_are_valid() ) {
        $j( "body" ).css( "cursor", "wait" );
        handle_verify_window_api( function( response ) {
            $j( "body" ).css( "cursor", "" );
            if( response == -1 ) {
                alert_user( "Cannot find user with the given fields. Please try again." );
            }

            else if( response == -2 ) {
                alert_user( "Cannot connect with database. Please try again." );
            }

            else {
                registration_fields = $j.parseJSON(response);
                if( "url" in registration_fields ) {
                    window.open( registration_fields["url"], "_self" );
                    return;
                }

                alert_btn.hide();
                next_btn.text( "Confirm" );
                next_btn.prop( "disabled", false );
                autofill_inputs();
                show_next_window();
            }
        });
    }

    else {
        next_btn.prop( "disabled", false );
    }
}

function verify_window_fields_are_valid() {
    /*
        Verify if all the input fields in the verify window are valid
        Verification == not empty
        @params:
            None
        @return:
            None
    */

    // check all before returning so that their respective validation feedback runs
    var none_empty = true;

    if( !input_field_filled( $j( "input#npi-number" ) ) ) {
        none_empty = false;
    }

    if( !input_field_filled( $j( "input#first-name" ) ) ) {
        none_empty = false;
    }

    if( !input_field_filled( $j( "input#last-name" ) ) ) {
        none_empty = false;
    }

    return none_empty;
}

function handle_verify_window_api( callback ) {
    /*
        Make an ajax call using the fields from the verify window.
        Used to check if the healthcare provider exists in NPPES
        @params:
            callback            -> function
        @return:
            None
    */

    var data = {
        "action":       "triwest_portal_check_nppes",
        "npi_number":   $j( "input#npi-number" ).val().trim(),
        "first_name":   $j( "input#first-name" ).val().trim(),
        "last_name":    $j( "input#last-name" ).val().trim()
    };

    $j.ajax({
        type:       "POST",
        url:        ajax_url,
        data:       data,
        timeout:    ajax_timeout,
        success:    callback,
        error:      function() {
            $j( "body" ).css( "cursor", "" );
            alert_user( "Request timeout. Please try verifying again." );
            next_btn.prop( "disabled", false );
        }
    });
}
