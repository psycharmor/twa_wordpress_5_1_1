<?php
$has_access = sfwd_lms_has_access($course_id);
global $course_pager_results;

do_action('learndash-focus-sidebar-before', $course_id, $user_id ); ?>

<div class="ld-focus-sidebar">
    <div class="ld-course-navigation-heading">

        <?php do_action('learndash-focus-sidebar-trigger-wrapper-before', $course_id, $user_id ); ?>

        <span class="ld-focus-sidebar-trigger">
            <?php do_action('learndash-focus-sidebar-trigger-before', $course_id, $user_id ); ?>
            <span class="ld-icon ld-icon-arrow-left"></span>
            <?php do_action('learndash-focus-sidebar-trigger-after', $course_id, $user_id ); ?>
        </span>

        <?php do_action('learndash-focus-sidebar-trigger-wrapper-after', $course_id, $user_id ); ?>

        <?php do_action('learndash-focus-sidebar-heading-before', $course_id, $user_id ); ?>

        <h3>
            <p id="ld-focus-mode-course-heading">
                <?php echo esc_html( get_the_title($course_id) ); ?>
            </p>
        </h3>
        <?php do_action('learndash-focus-sidebar-heading-after', $course_id, $user_id ); ?>
    </div>
</div> <!--/.ld-focus-sidebar-->

<?php do_action('learndash-focus-sidebar-after', $course_id, $user_id ); ?>
