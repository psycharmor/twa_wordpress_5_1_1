<?php
/*
    Name: create_user.php
    Description:
        Hold the function that handles the ajax call to register user with
        the given fields as long as the email doesn't already exist.

        Successfully created:       Give back 0
        User already exists:        Give back -1
*/

function ajax_create_user() {
    /*
        checks whether the user exists in the PAI database and if so don't continue
        @params: (from ajax request)
            email               -> string
        @return:
            none
    */

    if( isset( $_REQUEST ) ) {

        // check if the user or email exists
        $user_name = sanitize_user( $_REQUEST["email"], true );
        $user_id = username_exists( $user_name );
        if( !$user_id && !email_exists( sanitize_email( $_REQUEST["email"] ) ) ) {
            echo 0;
            wp_die();
        }

        else {
            echo -1;
            wp_die();
        }
    }

    else {
        echo -1;
        wp_die();
    }
}

add_action("wp_ajax_nopriv_triwest_portal_create_user", "ajax_create_user");   // user not logged
add_action("wp_ajax_triwest_portal_create_user", "ajax_create_user");          // user logged in
