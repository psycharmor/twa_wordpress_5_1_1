<?php
/*
    Name: register_window.php
    Description:
        Create the class that creates the register window section of the
        registration page
*/

namespace triwest_portal;

include_once ( get_stylesheet_directory() . "/modules/register/src/php/register_body.php" );

class Register_Window extends Register_Body {

    private $states;
    private $countries;

    public function __construct() {
        $this->states = array_map( "str_getcsv", file( get_stylesheet_directory() . "/modules/register/lib/states.csv" ) );
        $this->countries = array_map( "str_getcsv", file( get_stylesheet_directory() . "/modules/register/lib/countries.csv" ) );
    }

    public function create_window() {
        /*
            Create the window that asks for the following fields:
                Email                   phone number
                password                password confirm
                street address          city
                state       country     postal
                discipline              organization name
                department/division     title
                primary taxonomy code   primary taxonomy description
                secondary taxonomy code secondary taxonomy description
            @params:
                None
            @return:
                string
        */

        $title = "VA Training Provider Registration Step 2";
        $subtitle = "Please complete your registration below to ensure compliance with VA’s
                     provider profile. This is your final step for registration.";
        $info = "All the fields marked with * are required.";

        $content = "";

        $content .= "<div class='form-group window'>";
            $content .= "<div class='pai-blue-border'>";
            $content .= "</div>"; // pai-blue-border
            $content .= "<div class='pai-fields'>";
                $content .= $this->create_title_and_subtitle_element( $title, $subtitle );
                $content .= "<h3 class='pai-window-info'>$info</h3>";
                $content .= $this->create_multi_col_fields( array(
                    $this->create_input_element( "email", "email", "Email *", "JohnDoe@email.com", "Please enter a valid email address (E.g. 'JohnDoe@email.com')", "" ),
                    $this->create_input_element( "tel", "phone-number", "Phone Number *", "123-456-7890", "Please enter your phone number", "" )
                ), "" );
                $content .= $this->create_multi_col_fields( array(
                    $this->create_input_element( "password", "password", "Password *", "Enter your password", "Password must be at least 6 characters in length with no spaces and contain at least one lower case letter, one capital letter, and one number", "" ),
                    $this->create_input_element( "password", "password-confirm", "Confirm Your Password *", "Confirm your password", "Passwords must match","" )
                ), "" );
                $content .= $this->create_multi_col_fields( array(
                    $this->create_input_element( "text", "street-address", "Street Address *", "15 James Avenue", "Please enter your street address", "" ),
                    $this->create_input_element( "text", "city", "City *", "Enter your city", "Phoenix", "" )
                ), "" );
                $content .= $this->create_multi_col_fields( array(
                    $this->create_select_element( $this->states, "state", "State *", "Select your state", "Please select your state" ),
                    $this->create_select_element( $this->countries, "country", "Country *", "Enter your country", "Please enter your country" ),
                    $this->create_input_element( "number", "postal-code", "Postal Code *", "85001", "Please enter your postal code", "" )
                ), "" );
                $content .= $this->create_multi_col_fields( array(
                    $this->create_input_element( "text", "discipline", "Discipline *", "Enter your discipline", "Please enter your discipline", "" ),
                    $this->create_input_element( "text", "organization-name", "Organization Name *", "Enter your organization's name", "Please enter your organizations name", "" )
                ), "" );
                $content .= $this->create_multi_col_fields( array(
                    $this->create_input_element( "text", "department", "Department/Division *", "Enter your department/division", "Please enter your department/division", "" ),
                    $this->create_input_element( "text", "title", "Title *", "Enter your title", "Please enter your title", "" )
                ), "" );
                $content .= $this->create_multi_col_fields( array(
                    $this->create_input_element( "text", "taxonomy-code-pri", "Primary Taxonomy Code", "207X00000X", "", "disabled"),
                    $this->create_input_element( "text", "taxonomy-desc-pri", "Primary Taxonomy Description", "Orthopaedic Surgery", "", "disabled" )
                ), "" );
                $content .= $this->create_multi_col_fields( array(
                    $this->create_input_element( "text", "taxonomy-code-sec", "Secondary Taxonomy Code", "207X00000X", "", "disabled"),
                    $this->create_input_element( "text", "taxonomy-desc-sec", "Secondary Taxonomy Description", "Orthopaedic Surgery", "", "disabled" )
                ), "id='secondary-taxonomy-fields'" );
                $content .= "<input type='hidden' id='time-zone' name='time-zone' value=''>";
            $content .= "</div>"; //pai-fields
        $content .= "</div>"; // form-group window active

        return $content;
    }
}
