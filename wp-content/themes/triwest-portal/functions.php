<?php
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {

    $parent_style = 'parent-style'; // This is 'twentynineteen-style' for the Twenty Nineteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}

// functions that can be used by every module
include (STYLESHEETPATH . "/modules/src/php/general_custom_functions.php");
include (STYLESHEETPATH . "/modules/src/php/learndash_custom_functions.php");

// hooks and filters on wordpress actions
include (STYLESHEETPATH . "/modules/src/php/hooks_and_filters.php");

// rest API
include (STYLESHEETPATH . "/modules/src/php/rest_api.php");

// Sign Up
include (STYLESHEETPATH . "/modules/register/src/php/form/register_form.php");
include (STYLESHEETPATH . "/modules/register/src/php/ajax/check_nppes.php");
include (STYLESHEETPATH . "/modules/register/src/php/ajax/create_user.php");

// dashboard
include (STYLESHEETPATH . "/modules/dashboard/src/php/ajax/enroll_user.php");

// train api handling
include (STYLESHEETPATH . "/modules/train-api/src/train_api_script.php");
