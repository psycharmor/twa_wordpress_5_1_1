<?php

/*
    Name: register_form.php
    Description:
        Hold the function that handles the form submission of creating the user/
        After the user has been created, send the user to login
*/

function register_user() {
    /*
        Create the user and update their meta with the given fields
        @params: (from POST)
            npi_number          -> string   postal_code         -> string
            first_name          -> string   discipline          -> string
            last_name           -> string   organization        -> string
            email               -> string   department          -> string
            phone_number        -> string   title               -> string
            password            -> string   time_zone           -> string
            street_address      -> string   taxonomy_code_pri   -> string
            city                -> string   taxonomy_desc_pri   -> string
            state               -> string   opt_out_opioid      -> string
            country             -> string   opt_out_gcs         -> string

            IF AVAILABLE:
            taxonomy_code_sec   -> string
            taxonomy_desc_sec   -> string
    */

    if( isset( $_POST ) ) {
        $user_id = pai_create_user( $_POST );
        handle_opt_outs( $_POST, $user_id );
        update_user_meta( $user_id, "registration_completed", true );

        // send email to user
        $header = home_url( "/wp-content/uploads/2019/08/Email-Header-Final.png" );
        $footer = home_url( "/wp-content/uploads/2019/08/Email-Footer-Final.png" );

        $user_name = sanitize_user( $_POST["email"], true );

        $headers = array( "Content-Type: text/html; charset=UTF-8" );
        $message = "<img style='display: block; width: 100%;' src='$header' alt='TriWest header'><br><br>";
        $message .= "Welcome!<br><br>";
        $message .= "Your username is $user_name<br><br>";
        $message .= "Login to see your dashboard<br>";
        $message .= home_url('/my-dashboard/') . "<br><br>";
        $message .= "<img style='display: block; width: 100%;' src='$footer' alt='TriWest footer'>";
        wp_mail( sanitize_email( $_POST["email"] ), 'Welcome', $message, $headers );

        wp_safe_redirect( home_url('/my-dashboard/') );
        exit;
    }
}

function pai_create_user( &$post ) {
    /*
        create new user given fields from post
    */
    $user_name = sanitize_user( $post["email"], true );
    $user_id = username_exists( $user_name );
    if ( !$user_id && email_exists( sanitize_email( $post["email"] ) ) === false ) {
        $user_id = wp_create_user( $user_name, $post["password"], sanitize_email( $post["email"] ) );
        if (is_wp_error( $user_id )) {
            wp_safe_redirect( home_url() );
            exit;
        }


        // create an array that will hold the user meta to be added
        $user_meta = setup_user_meta( $post );

        foreach( $user_meta as $key => $value ) {
            update_user_meta( $user_id, $key, $value );
        }

        // add to $user_meta then create a user meta with all the fields
        // as the value
        $user_meta["email"] = sanitize_email( $post["email"] );
        $user_meta["login_name"] = $user_name;


        update_user_meta( $user_id, "triwest_fields", $user_meta );

        return $user_id;
    }

    else {
        wp_safe_redirect( home_url('/register/') );
        exit;
    }
}

function handle_opt_outs( &$post, $user_id ) {
    $opioid_id = 28;
    $gcs_id = 31;

    $opt_out_opioid = false;
    if( $post["opt-out-opioid"] == "on" ) {
        $opt_out_opioid = true;
    }
    $opt_out_gcs = false;
    if( $post["opt-out-gcs"] == "on" ) {
        $opt_out_gcs = true;
    }

    $opioid = array(
        "course_id"    => $opioid_id,
        "course_title" => get_the_title( $opioid_id ),
        "opt_out"      => $opt_out_opioid
    );

    $gcs = array(
        "course_id"    => $gcs_id,
        "course_title" => get_the_title( $gcs_id ),
        "opt_out"      => $opt_out_gcs
    );

    $opt_outs = array(
        $opioid,
        $gcs
    );

    update_user_meta( $user_id, "opt_outs", $opt_outs );

    if( $opt_out_opioid ) {
        enroll_and_auto_complete_course( $user_id, $opioid_id );
    }

    if( $opt_out_gcs ) {
        enroll_and_auto_complete_course( $user_id, $gcs_id );
    }
}

function setup_user_meta( &$post ) {
    /*
        Using the fields from the request array, create the array that will
        hold the user meta
        @params:
            $post            -> array
        @return:
            array( "meta_key" => "meta_value" )
    */

    $user_meta = array(
        "npi_number"            => intval( sanitize_text_field( $post["npi-number"] ) ),
        "first_name"            => sanitize_text_field( $post["first-name"] ),
        "last_name"             => sanitize_text_field( $post["last-name"] ),
        "phone_number"          => sanitize_text_field( $post["phone-number"] ),
        "street_address"        => sanitize_text_field( $post["street-address"] ),
        "city"                  => sanitize_text_field( $post["city"] ),
        "state"                 => sanitize_text_field( $post["state"] ),
        "country"               => sanitize_text_field( $post["country"] ),
        "postal_code"           => intval( sanitize_text_field( $post["postal-code"] ) ),
        "time_zone"             => sanitize_text_field( $post["time-zone"] ),
        "discipline"            => sanitize_text_field( $post["discipline"] ),
        "organization"          => sanitize_text_field( $post["organization-name"] ),
        "department"            => sanitize_text_field( $post["department"] ),
        "title"                 => sanitize_text_field( $post["title"] )
    );

    $user_meta["taxonomies"] = array();
    $user_meta["taxonomies"][] = array(
        "code"          => sanitize_text_field( $post["taxonomy-code-pri"] ),
        "description"   => sanitize_text_field( $post["taxonomy-desc-pri"] ),
        "primary"       => true
    );

    if( array_key_exists( "taxonomy-code-sec", $post ) ) {
        $user_meta["taxonomies"][] = array(
            "code"          => sanitize_text_field( $post["taxonomy-code-sec"] ),
            "description"   => sanitize_text_field( $post["taxonomy-desc-sec"] ),
            "primary"       => false
        );
    }

    return $user_meta;
}

function enroll_and_auto_complete_course( $user_id, $course_id ) {
    /*
        Enroll the user into the course and automatically complete the course
        for them
        @params:
            $course_id          -> int
        @return:
            None
    */

    // enroll user to course
    ld_update_course_access( $user_id, $course_id );

    // complete any lessons associated with the course if any
    $course_lessons = learndash_get_course_lessons_list( $course_id );
    foreach( $course_lessons as $lesson ) {
        learndash_process_mark_complete( $user_id, $lesson["post"]->ID );
    }

    // finally mark the course as complete
    learndash_process_mark_complete( $user_id, $course_id );
}

add_action( "admin_post_nopriv_register_form", "register_user" );
add_action( "admin_post_register_form", "register_user" );
