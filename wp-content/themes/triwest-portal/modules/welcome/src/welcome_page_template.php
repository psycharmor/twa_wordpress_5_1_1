<?php
/*
    Name: welcome_page_template.php
    Description:
        Extend the base page template to create the welcome home page
*/

namespace triwest_portal;

include_once ( get_stylesheet_directory() . "/modules/src/php/page_template.php" );

class Welcome_Page_Template extends Page_Template {

    public function __construct() {
        /*
            If the user is currently logged in, redirect them to their dashboard.
            Otherwise continue on making the page
        */

        if( is_user_logged_in() ) {
            wp_safe_redirect( home_url( "/my-dashboard/" ) );
            exit;
        }
    }

    private function include_files() {
        /*
            Include all the stylesheets and scripts needed for the Welcome page
            @params:
                None
            @return:
                None
        */

        $content = "<!-- FROM THE WELCOME TEMPLATE -->";

        $content .= "<link type='text/css' rel='stylesheet' href='" . get_stylesheet_directory_uri() . "/modules/welcome/src/css/style.css'>";

        return $content;
    }

    private function create_head_element() {
        /*
            Create the <head> element
            @params:
                None
            @return:
                string
        */

        $title = get_bloginfo( "name" );

        $content = "";

        $content .= "<head>";
            $content .= "<title>$title</title>";
            $content .= $this->include_base_files();
            $content .= $this->include_files();
        $content .= "</head>";

        return $content;
    }

    private function create_btn( $label, $url ) {
        /*
            Create the button element
            @params:
                label       -> string
                url         -> string
            @return:
                string
        */

        $content = "<a class='btn pai-welcome-btn' href='$url'>$label</a>";

        return $content;
    }

    private function create_body_element() {
        /*
            Create the <body> element
            @params:
                None
            @return:
                string
        */

        $content = "";

        $content .= "<body>";
            $content .= "<div class='pai-welcome-container'>";
                $content .= "<h1 class='pai-window-title'>Welcome!</h1>";
                $content .= "<div class='pai-btn-container'>";
                    $content .= $this->create_btn( "log In", wp_login_url() );
                    $content .= $this->create_btn( "Register Now", home_url( "/register/" ) );
                $content .= "</div>"; // pai-btn-container
            $content .= "</div>"; // pai-welcome-container
        $content .= "</body>";

        return $content;
    }

    public function create_page() {
        /*
            Create the entire page, including the <head> and the <body>.
            @params:
                None
            @return:
                None
        */

        $content = "";

        $content .= $this->create_head_element();
        $content .= $this->create_header();
        $content .= $this->create_body_element();

        echo html_entity_decode( $content );
    }
}
